<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Autocomplete</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container" id="root">

          <pre v-if="selected">You have selected: '@{{selected}}'</pre>

          <autocomplete :searcharray="people" @interface="selectedHero"></autocomplete>

        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>

        <script>
            new Vue({
              el: '#root',
              data: {
                selected: [],
                people: [{ id: '0011', name: 'Frodo'}, {id: '0012', name: 'Gandalf'}, {id: '0013', name: 'Galadriel'}, {id: '0014', name: 'Boromir'}, {id: '0015', name: 'Samwise'}, {id: '0016', name: 'Froome'}, {id: '0017', name: 'Frog'}]
              },
              methods: {
                selectedHero (event) {
                  this.selected = event;
                }
              }
            });
        </script>
    </body>
</html>
